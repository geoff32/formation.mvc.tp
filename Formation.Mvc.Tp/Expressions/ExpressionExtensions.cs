﻿using System;
using System.Linq.Expressions;

namespace Formation.Mvc.Tp.Expressions
{
    public static class ExpressionExtensions
    {
        public static string GetPropertyName<TSource, TProperty>(this Expression<Func<TSource, TProperty>> expression)
        {
            return (expression.Body as MemberExpression).Member.Name;
        }
    }
}