﻿using Formation.Mvc.Tp.DataAnnotations;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Formation.Mvc.Tp.Models
{
    public class ProductModel
    {
        public int Id { get; set; }
        //[Required]
        //[DisplayName("Product name")]
        public string Name { get; set; }
        //[StringLength(200, ErrorMessage = "La description est trop longue")]
        //[RequiredIfGreaterThan(nameof(Price), 10)]
        //[DisplayName("Description")]
        public string Description { get; set; }
        //[Required]
        //[DisplayName("Unit price")]
        public decimal Price { get; set; }
        //[DisplayName("Activated"), ReadOnly(true)]
        //[Display]
        public bool Active { get; set; }
        public ProductTypeModel Type { get; set; }
    }
}