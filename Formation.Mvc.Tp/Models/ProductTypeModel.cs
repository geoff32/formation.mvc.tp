﻿namespace Formation.Mvc.Tp.Models
{
    public class ProductTypeModel
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public ProductTypeModel Parent { get; set; }
    }
}