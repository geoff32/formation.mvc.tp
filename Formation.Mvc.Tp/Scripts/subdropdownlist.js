﻿(function ($) {
    $.subdropdownlist = function (container) {
        this.element = container.find("[data-sub-element]");
        this.parent = container.find("[data-sub-parent]");
        this.element.data("sub-parent-ref", this.parent);
        this.parent.data("sub-element-ref", this.element);
        this.initialize();
    };

    $.subdropdownlist.prototype = {
        val: function (v) {
            return this.element.val(v);
        },
        initialize: function () {
            this.parent.on("change", function () {
                $(this).data("sub-element-ref").trigger("load-datasource", [$(this).val()]);
            });

            this.element.on("load-datasource", function (e, parentVal) {
                var loadUrl = $(this).data("load-url") + parentVal;
                var that = this;
                $.get(loadUrl, function (data) {
                    var dataContainer = $("<div></div>").append(data);
                    $(that).html(dataContainer.find("[data-sub-container] > [data-sub-element]").html());
                });
            });
        }
    };

    $.subdropdownlist.initialize = function (container) {
        if (!!container) {
            container.subdropdownlist();
        }
        else {
            $("[data-sub-container]").each(function (index, elt) {
                $(elt).subdropdownlist();
            });
        }
    };

    $.fn.subdropdownlist = function () {
        return new $.subdropdownlist(this);
    };

    $.subdropdownlist.initialize();
}(jQuery));