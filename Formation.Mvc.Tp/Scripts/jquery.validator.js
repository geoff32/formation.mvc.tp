﻿(function($) {
    $.validator.unobtrusive.adapters.add('requiredifgreaterthan', ['propertyname', 'minvalue'], function (options) {
        options.rules.requiredifgreaterthan = options.params;
        options.messages.requiredifgreaterthan = options.message;
    });

    $.validator.addMethod('requiredifgreaterthan', function (value, element, params) {
        var val = parseInt($(this.currentForm).find('#' + params.propertyname).val());
        return !!value || val < params.minvalue;
    })
})(jQuery);