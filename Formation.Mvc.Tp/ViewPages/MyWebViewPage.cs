﻿using Formation.Mvc.Tp.Resources;
using System.Web.Mvc;

namespace Formation.Mvc.Tp.ViewPages
{
    public abstract class MyWebViewPage<TModel> : WebViewPage<TModel>
    {
        private readonly MyResourceManager _resourceManager;

        protected MyWebViewPage()
        {
            _resourceManager = new MyResourceManager("fr-FR", "en-US");
        }

        protected string T(string key) => _resourceManager.Get(key);

        protected T GetService<T>()
        {
            return DependencyResolver.Current.GetService<T>();
        }
    }
}