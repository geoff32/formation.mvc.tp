﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Formation.Mvc.Tp.DataAnnotations
{
    public class RequiredIfGreaterThanAttribute : ValidationAttribute, IClientValidatable
    {
        private readonly string _propertyName;
        private readonly int _minValue;

        public RequiredIfGreaterThanAttribute(string propertyName, int minValue)
        {
            _propertyName = propertyName;
            _minValue = minValue;
            ErrorMessage = $"Required if {_propertyName} greater than {_minValue}";
        }
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            return value == null
                && GetIntValue(validationContext.ObjectType.GetProperty(_propertyName).GetValue(validationContext.ObjectInstance)) >= _minValue
                ? new ValidationResult(BuildErrorMessage(validationContext.ObjectType)) : ValidationResult.Success;
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var rule = new ModelClientValidationRule
            {
                ErrorMessage = BuildErrorMessage(metadata.ContainerType),
                ValidationType = "requiredifgreaterthan"
            };
            rule.ValidationParameters.Add("propertyname", _propertyName);
            rule.ValidationParameters.Add("minvalue", _minValue);

            yield return rule;
        }

        private string BuildErrorMessage(Type type)
        {
            var propertyMeta = ModelMetadataProviders.Current.GetMetadataForProperty(() => Activator.CreateInstance(type), type, _propertyName);

            return $"Required if {propertyMeta.DisplayName} greater than {_minValue}";
        }

        private int GetIntValue(object obj)
        {
            switch (obj)
            {
                case decimal d:
                    return (int)d;
                case double dbl:
                    return (int)dbl;
            }

            return (int)obj;
        }
    }
}