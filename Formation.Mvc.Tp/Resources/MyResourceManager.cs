﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Formation.Mvc.Tp.Resources
{
    public class MyResourceManager
    {
        private readonly Dictionary<string, Dictionary<string, string>> _resources;

        public MyResourceManager(params string[] cultures)
            => _resources = (cultures ?? new string[] { }).ToDictionary(c => c, LoadCulture);

        protected virtual Dictionary<string, string> LoadCulture(string culture)
            => Enumerable.Range(0, 10).ToDictionary(i => $"key{i}", i => $"val{i} {culture}");

        public string Get(string key)
            => Get(key, System.Threading.Thread.CurrentThread.CurrentUICulture);

        public string Get(string key, CultureInfo culture)
            => _resources.TryGetValue(culture.Name, out var cultureResources)
                && cultureResources.TryGetValue(key, out var translation)
                ? translation : null;
    }
}