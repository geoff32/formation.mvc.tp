﻿using Formation.Mvc.Tp.Models;
using Formation.Mvc.Tp.Services.Abstractions;
using System.Linq;
using System.Web.Mvc;

namespace Formation.Mvc.Tp.Controllers
{
    public class ProductController : Controller
    {
        private readonly IProductService _productService;

        public ProductController(IProductService productService) => _productService = productService;

        public ActionResult Index() => View(_productService.GetAllProducts());

        [HttpGet]
        public ActionResult Details(int id) => View(_productService.Get(id));

        [HttpGet]
        public ActionResult Edit(int id) => View(_productService.Get(id));

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ProductModel product)
        {
            if (ModelState.IsValid)
            {
                product.Type = product.Type?.Id == null ? null : _productService.GetProductType(product.Type.Id);
                _productService.Update(product);
                return RedirectToAction("Index");
            }
            return View(product);
        }

        [HttpGet]
        public ActionResult Create() => View();

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ProductModel product)
        {
            if (ModelState.IsValid)
            {
                product.Type = product.Type?.Id == null ? null : _productService.GetProductType(product.Type.Id);
                _productService.Create(product);
                return RedirectToAction("Index");
            }
            return View(product);
        }

        [HttpGet]
        public ActionResult Delete(int id) => View(_productService.Get(id));

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(ProductModel product)
        {
            _productService.Delete(product.Id);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult GetEmptySubType(int productTypeId) => PartialView("EditorTemplates/ProductTypeModel", _productService.GetProductSubTypes(productTypeId).FirstOrDefault());
    }
}