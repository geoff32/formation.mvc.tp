﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;
using System.Xml;

namespace Formation.Mvc.Tp.Metadata
{
    public class XmlModelMetadataProvider : DataAnnotationsModelMetadataProvider
    {
        private readonly Dictionary<Type, Dictionary<string, IEnumerable<Attribute>>> _metadata;

        public XmlModelMetadataProvider(XmlMetadata xmlMetadata)
        {
            _metadata = LoadMetadata(xmlMetadata);
        }

        protected override ModelMetadata CreateMetadata(IEnumerable<Attribute> attributes, Type containerType, Func<object> modelAccessor, Type modelType, string propertyName)
        {
            var metaAttributes = containerType != null && !string.IsNullOrEmpty(propertyName)
                && _metadata.TryGetValue(containerType, out var properties)
                && properties.TryGetValue(propertyName, out var attrCollection)
                ? attrCollection : attributes;
            return base.CreateMetadata(metaAttributes, containerType, modelAccessor, modelType, propertyName);
        }

        private Dictionary<Type, Dictionary<string, IEnumerable<Attribute>>> LoadMetadata(XmlMetadata xmlMetadata)
        {
            var metadata = new Dictionary<Type, Dictionary<string, IEnumerable<Attribute>>>();
            foreach (var type in xmlMetadata.GetTypes())
            {
                metadata.Add(type, new Dictionary<string, IEnumerable<Attribute>>());
                var propertyNodes = xmlMetadata.GetNodes(type);
                if (propertyNodes != null)
                {
                    foreach (XmlElement propertyNode in propertyNodes)
                    {
                        metadata[type].Add(propertyNode.Name, propertyNode.Attributes.Cast<XmlAttribute>()
                            .Select(GetAttribute)
                            .Where(attr => attr != null).ToList());
                    }
                }
            }

            return metadata;
        }

        private Attribute GetAttribute(XmlAttribute xmlAttribute)
        {
            switch (xmlAttribute.Name)
            {
                case "DisplayName":
                    return new DisplayNameAttribute(xmlAttribute.Value);
                case "ReadOnly":
                    if (bool.TryParse(xmlAttribute.Value, out var ro))
                    {
                        return new ReadOnlyAttribute(ro);
                    }
                    return null;
                default:
                    return null;
            }
        }
    }
}