﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Xml;

namespace Formation.Mvc.Tp.Metadata
{
    public class XmlMetadata
    {
        private readonly Dictionary<Type, XmlNodeList> _nodes;

        public XmlMetadata(string xmlFile)
        {
            _nodes = LoadNodes(xmlFile);
        }

        public IEnumerable<Type> GetTypes() => _nodes.Keys.ToList();

        public XmlNodeList GetNodes(Type type)
        {
            return _nodes.TryGetValue(type, out var xmlNodeList) ? xmlNodeList : null;
        }

        private Dictionary<Type, XmlNodeList> LoadNodes(string xmlFile)
        {
            var assembly = Assembly.GetExecutingAssembly();
            var doc = new XmlDocument();
            doc.Load(HttpContext.Current.Server.MapPath(xmlFile));

            var metadata = new Dictionary<Type, XmlNodeList>();
            foreach (XmlNode node in doc.DocumentElement.ChildNodes)
            {
                var type = assembly.GetType(node.Name);
                metadata.Add(type, node.ChildNodes);
            }

            return metadata;
        }
    }
}