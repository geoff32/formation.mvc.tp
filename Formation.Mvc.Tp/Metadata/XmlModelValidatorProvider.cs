﻿using Formation.Mvc.Tp.DataAnnotations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using System.Xml;

namespace Formation.Mvc.Tp.Metadata
{
    public class XmlModelValidatorProvider : DataAnnotationsModelValidatorProvider
    {
        private readonly Dictionary<Type, Dictionary<string, IEnumerable<ValidationAttribute>>> _validators;

        public XmlModelValidatorProvider(XmlMetadata xmlMetadata)
        {
            _validators = LoadValidators(xmlMetadata);
        }

        protected override IEnumerable<ModelValidator> GetValidators(ModelMetadata metadata, ControllerContext context, IEnumerable<Attribute> attributes)
        {
            var metaAttributes = metadata.ContainerType != null && !string.IsNullOrEmpty(metadata.PropertyName)
                && _validators.TryGetValue(metadata.ContainerType, out var properties)
                && properties.TryGetValue(metadata.PropertyName, out var attrCollection)
                ? attrCollection : attributes;

            return base.GetValidators(metadata, context, metaAttributes);
        }

        private Dictionary<Type, Dictionary<string, IEnumerable<ValidationAttribute>>> LoadValidators(XmlMetadata xmlMetadata)
        {
            var validators = new Dictionary<Type, Dictionary<string, IEnumerable<ValidationAttribute>>>();
            foreach (var type in xmlMetadata.GetTypes())
            {
                validators.Add(type, new Dictionary<string, IEnumerable<ValidationAttribute>>());
                var propertyNodes = xmlMetadata.GetNodes(type);
                if (propertyNodes != null)
                {
                    foreach (XmlElement propertyNode in propertyNodes)
                    {
                        var xmlValidators = propertyNode.GetElementsByTagName("Validators")
                            .Cast<XmlNode>().SingleOrDefault()?.ChildNodes;

                        if (xmlValidators != null)
                        {
                            validators[type].Add(propertyNode.Name, xmlValidators.Cast<XmlElement>()
                                .Select(GetValidator)
                                .Where(attr => attr != null).ToList());
                        }
                    }
                }
            }

            return validators;
        }

        private ValidationAttribute GetValidator(XmlElement xmlElement)
        {
            switch (xmlElement.Name)
            {
                case "Required":
                    if (IsValidatorActive(xmlElement))
                    {
                        return new RequiredAttribute();
                    }
                    break;
                case "RequiredIfGreaterThan":
                    var propertyName = xmlElement.GetAttribute("PropertyName");
                    if (IsValidatorActive(xmlElement)
                        && !string.IsNullOrEmpty(propertyName)
                        && int.TryParse(xmlElement.GetAttribute("MinValue"), out var minValue))
                    {
                        return new RequiredIfGreaterThanAttribute(propertyName, minValue);
                    }
                    break;
                case "StringLength":
                    if (IsValidatorActive(xmlElement)
                        && int.TryParse(xmlElement.GetAttribute("MaxLength"), out var maxLength))
                    {
                        return new StringLengthAttribute(maxLength) { ErrorMessage = xmlElement.GetAttribute("ErrorMessage") };
                    }
                    break;
                default:
                    break;
            }

            return null;
        }

        private bool IsValidatorActive(XmlElement xmlElement)
        {
            return bool.TryParse(xmlElement.InnerText, out var active) && active;
        }
    }
}