﻿using Formation.Mvc.Tp.Expressions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;

namespace Formation.Mvc.Tp.HtmlHelpers
{
    public static class DropDownListHelper
    {
        public static MvcHtmlString DropDownListFor<TModel, TProperty, TSource, TText>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> propertyExpression,
            Expression<Func<TModel, IEnumerable<TSource>>> datasource,
            Expression<Func<TSource, TProperty>> dataExpression,
            Expression<Func<TSource, TText>> textExpression,
            RouteValueDictionary htmlAttributes)
        {
            return htmlHelper.DropDownListFor(propertyExpression, datasource, dataExpression.GetPropertyName(), textExpression.GetPropertyName(), htmlAttributes);
        }

        public static MvcHtmlString DropDownListFor<TModel, TProperty, TSource, TText>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> propertyExpression,
            Expression<Func<TModel, IEnumerable<TSource>>> datasource,
            Expression<Func<TSource, TProperty>> dataExpression,
            Expression<Func<TSource, TText>> textExpression,
            object htmlAttributes = null)
        {
            return htmlHelper.DropDownListFor(propertyExpression, datasource, dataExpression.GetPropertyName(), textExpression.GetPropertyName(), htmlAttributes);
        }

        public static MvcHtmlString DropDownListFor<TModel, TProperty, TSource>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> propertyExpression,
            Expression<Func<TModel, IEnumerable<TSource>>> datasource,
            string dataPropertyName,
            string textPropertyName,
            RouteValueDictionary htmlAttributes)
        {
            return htmlHelper.DropDownListFor(
                propertyExpression,
                new SelectList(datasource.Compile().Invoke(htmlHelper.ViewData.Model), dataPropertyName, textPropertyName),
                htmlAttributes
            );
        }

        public static MvcHtmlString DropDownListFor<TModel, TProperty, TSource>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> propertyExpression,
            Expression<Func<TModel, IEnumerable<TSource>>> datasource,
            string dataPropertyName,
            string textPropertyName,
            object htmlAttributes = null)
        {
            return htmlHelper.DropDownListFor(
                propertyExpression,
                new SelectList(datasource.Compile().Invoke(htmlHelper.ViewData.Model), dataPropertyName, textPropertyName),
                htmlAttributes
            );
        }

        public static MvcHtmlString DropDownList<TSource, TData, TText>(
            this HtmlHelper htmlHelper,
            IEnumerable<TSource> datasource,
            Expression<Func<TSource, TData>> dataExpression,
            Expression<Func<TSource, TText>> textExpression,
            TData value = default(TData),
            object htmlAttributes = null)
        {
            return htmlHelper.DropDownList(datasource, dataExpression, textExpression, value, HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));
        }

        public static MvcHtmlString DropDownList<TSource, TData, TText>(
            this HtmlHelper htmlHelper,
            IEnumerable<TSource> datasource,
            Expression<Func<TSource, TData>> dataExpression,
            Expression<Func<TSource, TText>> textExpression,
            TData value = default(TData),
            RouteValueDictionary htmlAttributes = null)
        {
            var select = new TagBuilder("select");
            if (htmlAttributes != null)
            {
                foreach (var attr in htmlAttributes)
                {
                    select.Attributes.Add(attr.Key, attr.Value?.ToString());
                }
            }
            var getData = dataExpression.Compile();
            var getText = textExpression.Compile();
            select.InnerHtml += string.Join("",
                datasource.Select(item =>
                {
                    var opt = new TagBuilder("option");
                    var data = getData(item);
                    opt.Attributes.Add("value", data.ToString());
                    if (data.Equals(value))
                    {
                        opt.Attributes.Add("selected", "selected");
                    }
                    opt.SetInnerText(getText(item).ToString());

                    return opt.ToString();
                }));

            return new MvcHtmlString(select.ToString());
        }
    }
}