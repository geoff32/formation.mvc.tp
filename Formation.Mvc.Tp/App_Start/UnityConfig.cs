using Formation.Mvc.Tp.Author.Services;
using Formation.Mvc.Tp.Author.Services.Abstractions;
using Formation.Mvc.Tp.Services;
using Formation.Mvc.Tp.Services.Abstractions;
using System;

using Unity;

namespace Formation.Mvc.Tp
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public static class UnityConfig
    {
        #region Unity Container
        private static Lazy<IUnityContainer> container =
          new Lazy<IUnityContainer>(() =>
          {
              var c = new UnityContainer();
              RegisterTypes(c);
              return c;
          });

        /// <summary>
        /// Configured Unity Container.
        /// </summary>
        public static IUnityContainer Container => container.Value;
        #endregion

        /// <summary>
        /// Registers the type mappings with the Unity container.
        /// </summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>
        /// There is no need to register concrete types such as controllers or
        /// API controllers (unless you want to change the defaults), as Unity
        /// allows resolving a concrete type even if it was not previously
        /// registered.
        /// </remarks>
        public static void RegisterTypes(IUnityContainer container)
        {
            container.RegisterSingleton<IProductService, ProductService>();
            container.RegisterSingleton<IAuthorService, AuthorService>();
        }
    }
}