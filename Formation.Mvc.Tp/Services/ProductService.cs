﻿using Formation.Mvc.Tp.Models;
using Formation.Mvc.Tp.Services.Abstractions;
using System.Collections.Generic;
using System.Linq;

namespace Formation.Mvc.Tp.Services
{
    public class ProductService : IProductService
    {
        private readonly Dictionary<int, ProductModel> _products;
        private readonly Dictionary<int, ProductTypeModel> _productTypes;

        public ProductService()
        {
            _products = new Dictionary<int, ProductModel>();

            var emptyRoot = new ProductTypeModel { Id = 0, Text = "Empty" };
            var bookRoot = new ProductTypeModel { Id = 1, Text = "Book" };
            var movieRoot = new ProductTypeModel { Id = 2, Text = "Movie" };
            _productTypes = new List<ProductTypeModel>
            {
                emptyRoot,
                bookRoot,
                movieRoot,
                new ProductTypeModel { Id = 3, Text = "Empty", Parent = emptyRoot },
                new ProductTypeModel { Id = 4, Text = "Empty", Parent = bookRoot },
                new ProductTypeModel { Id = 5, Text = "Paper", Parent = bookRoot },
                new ProductTypeModel { Id = 6, Text = "Digital", Parent = bookRoot },
                new ProductTypeModel { Id = 7, Text = "Empty", Parent = movieRoot },
                new ProductTypeModel { Id = 8, Text = "Dvd", Parent = movieRoot },
                new ProductTypeModel { Id = 9, Text = "Bluray", Parent = movieRoot }
            }.ToDictionary(ptm => ptm.Id);
        }

        public IEnumerable<ProductModel> GetAllProducts() => _products.Values;

        public bool Create(ProductModel product)
        {
            product.Id = _products.Keys.Any() ? _products.Keys.Max() + 1 : 1;
            _products.Add(product.Id, product);

            return true;
        }

        public ProductModel Get(int id) => _products.TryGetValue(id, out var product) ? product : null;

        public bool Update(ProductModel product)
        {
            _products[product.Id] = product;
            return true;
        }

        public bool Delete(int id) => _products.Remove(id);

        public ProductTypeModel GetProductType(int productTypeId)
            => _productTypes.TryGetValue(productTypeId, out var productTypeModel) ? productTypeModel : null;

        public IEnumerable<ProductTypeModel> GetRootProductTypes()
            => _productTypes.Values.Where(pt => pt.Parent == null);

        public IEnumerable<ProductTypeModel> GetProductSubTypes(int productTypeId)
            => _productTypes.Values.Where(pt => pt.Parent != null && pt.Parent.Id == productTypeId);
    }
}