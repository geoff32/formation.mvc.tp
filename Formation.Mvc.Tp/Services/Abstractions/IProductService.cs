﻿using Formation.Mvc.Tp.Models;
using System.Collections.Generic;

namespace Formation.Mvc.Tp.Services.Abstractions
{
    public interface IProductService
    {
        IEnumerable<ProductModel> GetAllProducts();
        ProductModel Get(int id);
        bool Update(ProductModel product);
        bool Create(ProductModel product);
        bool Delete(int id);
        ProductTypeModel GetProductType(int productTypeId);
        IEnumerable<ProductTypeModel> GetRootProductTypes();
        IEnumerable<ProductTypeModel> GetProductSubTypes(int productTypeId);
    }
}
