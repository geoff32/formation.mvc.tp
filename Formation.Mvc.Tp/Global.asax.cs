﻿using Formation.Mvc.Tp.Metadata;
using System.Web.Mvc;
using System.Web.Routing;

namespace Formation.Mvc.Tp
{
    public class MvcApplication : System.Web.HttpApplication
    {
        private const string XML_FILE = "Models/metadata.xml";

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            var viewEngine = new RazorViewEngine();
            viewEngine.AreaViewLocationFormats = new[] { "~/Views/Areas/{2}/{1}/{0}.cshtml", "~/Views/Areas/{2}/Shared/{1}/{0}.cshtml" };
            ViewEngines.Engines.Add(viewEngine);

            var xmlMetadata = new XmlMetadata(XML_FILE);
            ModelMetadataProviders.Current = new XmlModelMetadataProvider(xmlMetadata);
            ModelValidatorProviders.Providers.Clear();
            ModelValidatorProviders.Providers.Add(new XmlModelValidatorProvider(xmlMetadata));
        }
    }
}
