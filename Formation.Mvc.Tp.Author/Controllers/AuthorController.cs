﻿using Formation.Mvc.Tp.Author.Models;
using Formation.Mvc.Tp.Author.Services.Abstractions;
using System.Web.Mvc;

namespace Formation.Mvc.Tp.Author.Controllers
{
    public class AuthorController : Controller
    {
        private readonly IAuthorService _authorService;

        public AuthorController(IAuthorService authorService) => _authorService = authorService;

        [HttpGet]
        public ActionResult AuthorRequest() => View(Session["AuthorRequest"] as AuthorRequestModel);

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AuthorRequest(AuthorRequestModel authorRequest)
        {
            if (ModelState.IsValid && _authorService.AuthorRequest(authorRequest))
            {
                Session.Remove("AuthorRequest");
                return RedirectToAction(nameof(Index));
            }

            Session["AuthorRequest"] = authorRequest;
            return View(authorRequest);
        }

        [HttpGet]
        public ActionResult Index() => View(_authorService.GetAllAuthorRequests());
    }
}