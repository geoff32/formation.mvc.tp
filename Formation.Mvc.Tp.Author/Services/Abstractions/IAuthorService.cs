﻿using Formation.Mvc.Tp.Author.Models;
using System.Collections.Generic;

namespace Formation.Mvc.Tp.Author.Services.Abstractions
{
    public interface IAuthorService
    {
        IEnumerable<AuthorRequestModel> GetAllAuthorRequests();
        bool AuthorRequest(AuthorRequestModel authorRequest);
    }
}
