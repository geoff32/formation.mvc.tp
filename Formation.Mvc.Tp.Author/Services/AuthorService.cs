﻿using Formation.Mvc.Tp.Author.Models;
using Formation.Mvc.Tp.Author.Services.Abstractions;
using System.Collections.Generic;

namespace Formation.Mvc.Tp.Author.Services
{
    public class AuthorService : IAuthorService
    {
        private readonly List<AuthorRequestModel> _authorRequestCollection;

        public AuthorService() => _authorRequestCollection = new List<AuthorRequestModel>();

        public IEnumerable<AuthorRequestModel> GetAllAuthorRequests() => _authorRequestCollection;

        public bool AuthorRequest(AuthorRequestModel authorRequest)
        {
            _authorRequestCollection.Add(authorRequest);
            return true;
        }
    }
}