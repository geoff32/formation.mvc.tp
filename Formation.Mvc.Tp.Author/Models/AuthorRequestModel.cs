﻿using System.ComponentModel.DataAnnotations;

namespace Formation.Mvc.Tp.Author.Models
{
    public class AuthorRequestModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string Motivation { get; set; }
    }
}