﻿using System.Linq;
using System.Web.Mvc;

namespace Formation.Mvc.Tp.Author
{
    public class AuthorAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Author";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                name: "AuthorRequest",
                url: "devenez-auteur",
                defaults: new { controller = "Author", action = "AuthorRequest" }
            );

            context.MapRoute(
                name: "AuthorDefault",
                url: "author/{action}/{id}",
                defaults: new { controller = "Author", action = "Index", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "Author_default",
                "Author/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}