﻿using Formation.Mvc.Tp.Controllers;
using Formation.Mvc.Tp.Models;
using Formation.Mvc.Tp.Services.Abstractions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Formation.Mvc.Tp.Tests.Controllers
{
    [TestClass]
    public class ProductControllerTest
    {
        [TestMethod]
        public void Route_ProductList_Check()
        {
            var httpContext = new Mock<HttpContextBase>();
            httpContext.Setup(ctx => ctx.Request.AppRelativeCurrentExecutionFilePath)
                .Returns("~/product");

            var routeCollection = new RouteCollection();
            RouteConfig.RegisterRoutes(routeCollection);

            var routeData = routeCollection.GetRouteData(httpContext.Object);

            Assert.IsNotNull(routeData);

            Assert.AreEqual("Product", routeData.Values["controller"]);
            Assert.AreEqual("Index", routeData.Values["action"]);
        }

        [TestMethod]
        public void Route_ProductDetail_Check()
        {
            var httpContext = new Mock<HttpContextBase>();
            httpContext.Setup(ctx => ctx.Request.AppRelativeCurrentExecutionFilePath)
                .Returns("~/product/details/2");

            var routeCollection = new RouteCollection();
            RouteConfig.RegisterRoutes(routeCollection);

            var routeData = routeCollection.GetRouteData(httpContext.Object);

            Assert.IsNotNull(routeData);

            Assert.AreEqual("product", routeData.Values["controller"]);
            Assert.AreEqual("details", routeData.Values["action"]);
            Assert.AreEqual("2", routeData.Values["id"]);
        }

        [TestMethod]
        public void ProductList_ShouldReturnSameAsService()
        {
            var productService = new Mock<IProductService>();
            var productList = new List<ProductModel> { Mock.Of<ProductModel>() };
            productService.Setup(s => s.GetAllProducts())
                .Returns(productList);

            var controller = new ProductController(productService.Object);
            var result = controller.Index() as ViewResult;

            Assert.IsNotNull(result);
            Assert.AreEqual(productList, result.Model);
        }
    }
}
